CREATE TABLE
IF NOT EXISTS
`site_segment` (
  `id`         INTEGER PRIMARY KEY,
  `site_id`    INTEGER NOT NULL,
  `name`       TEXT    NOT NULL,
  `segment`   TEXT,
  `created_at` TEXT    DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TEXT    DEFAULT CURRENT_TIMESTAMP
);
