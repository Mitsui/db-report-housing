<?php

require_once("./vendor/autoload.php");

use Stereotype\Service\Client;
use Stereotype\Service\Analytics;

use Stereotype\ReportSet;

$client = Client::createServiceClient();
$analytics = new Analytics($client);

// $report = new ReportSet($analytics, "00000000");
$report = new ReportSet($analytics);
$report->run()->save();
