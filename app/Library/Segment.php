<?php

namespace Stereotype\Library;

class Segment
{
    /**
     * PCサイトを閲覧
     */
    const ViewPcSite = "sessions::condition::!ga:pagePath=~/sp/";

    /**
     * スマホサイトを閲覧
     */
    const ViewSpSite = "sessions::condition::ga:pagePath=~/sp/";

    /**
     * 広告からの流入
     */
    const FromAd = "sessions::condition::ga:medium=~^(cpc|ppc|cpa|cpm|cpv|cpp)$";

    /**
     * 広告以外からの流入
     */
    const FromNotAd = "sessions::condition::!ga:medium=~^(cpc|ppc|cpa|cpm|cpv|cpp)$";

    /**
     * Facebook広告からの流入
     */
    const FromFacebookAd = "sessions::condition::ga:source=~facebook;ga:medium=~^(cpc|ppc|cpa|cpm|cpv|cpp)$";

    /**
     * Google広告からの流入
     */
    const FromGoogleAd = "sessions::condition::ga:source=~google|gdn;ga:medium=~^(cpc|ppc|cpa|cpm|cpv|cpp)$";

    /**
     * Yahoo広告からの流入
     */
    const FromYahooAd = "sessions::condition::ga:source=~yahoo|ydn;ga:medium=~^(cpc|ppc|cpa|cpm|cpv|cpp)$";


    /**
     * Facebookからの流入
     */
    const FromFacebook = "sessions::condition::ga:source=~facebook";

    /**
     * コンバージョンしたユーザー
     */
    const Converters = "sessions::condition::ga:goalCompletionsAll>0";

    /**
     * PCからアクセス
     */
    const UsePc = "sessions::condition::ga:deviceCategory==desktop";

    /**
     * スマホ・タブレットからアクセス
     */
    const UseSp = "sessions::condition::ga:deviceCategory[]mobile|tablet";

    /**
     * リファラースパム除外
     */
    const AntiSpam = "sessions::condition::ga:language!=(not set);ga:country==Japan;ga:browser!=(not set)";


    /**
     * 検索用ハッシュマップ
     */
    private $segments = [
        "view_pc_site"      => self::ViewPcSite,
        "view_sp_site"      => self::ViewSpSite,
        "from_ad"           => self::FromAd,
        "from_not_ad"       => self::FromNotAd,
        "from_facebook_ad"  => self::FromFacebookAd,
        "from_google_ad"    => self::FromGoogleAd,
        "from_yahoo_ad"     => self::FromYahooAd,
        "from_facebook"     => self::FromFacebook,
        "converters"        => self::Converters,
        "use_pc"            => self::UsePc,
        "use_sp"            => self::UseSp,
        "anti_spam"         => self::AntiSpam,
        "not_spam"          => self::AntiSpam,
    ];

    /**
     * データ取得用
     *
     * @throws if 要求されたセグメント名が見つからない場合
     */
    public function __get($name)
    {
        if (isset($this->segments[$name])) {
            return $this->segments[$name];
        } else {
            throw new \Exception("Segment '$name' not found in Segment Library.");
        }
    }
}
