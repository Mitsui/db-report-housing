<?php

namespace Stereotype\Library;

class Filters
{
    /**
     * 直アクセス
     */
    const Direct = "ga:source==(direct)";

    /**
     * 直アクセス以外
     */
    const NotDirect = "ga:source!=(direct)";

    /**
     * 検索エンジンからの流入
     */
    const FromOrganicSearch = "ga:medium==organic,ga:source=~(image\.search\.yahoo\.co\.jp|bing\.com|websearch\.excite\.co\.jp|jword\.jp|search\.azby\.fmworld\.net)$,ga:source=~(search\.nifty\.com|search\.fenrir-inc\.com|search\.fooooo\.com|search\.nos\.adingo\.jp)$,ga:source=~(search\.plushome\.aswidget\.com|zapmeta\.jp|int\.search\.tb\.ask\.com|gws\.cybozu\.net|search\.cybozu\.net)$,ga:source=~(spsearch\.cybozu\.net|home\.kingsoft\.jp|jp\.hao123\.com|loco\.yahoo\.co\.jp|paconet\.starthome\.jp)$,ga:source=~(s\.cube-soft\.jp|search\.myjcom\.jp|so-net\.ne\.jp|accountpage\.line\.me|search\.dressapp\.aswidget\.com)$,ga:source=~(search\.jiqoo\.jp|www2\.delta-search\.com|r\.duckduckgo\.com|search\.livedoor\.com|smaken\.jp)$";

    /**
     * 検索エンジン以外からの流入
     */
    const FromNotOrganicSearch = "ga:medium!=organic;ga:source!~(image\.search\.yahoo\.co\.jp|bing\.com|websearch\.excite\.co\.jp|jword\.jp|search\.azby\.fmworld\.net)$;ga:source!~(search\.nifty\.com|search\.fenrir-inc\.com|search\.fooooo\.com|search\.nos\.adingo\.jp)$;ga:source!~(search\.plushome\.aswidget\.com|zapmeta\.jp|int\.search\.tb\.ask\.com|gws\.cybozu\.net|search\.cybozu\.net)$;ga:source!~(spsearch\.cybozu\.net|home\.kingsoft\.jp|jp\.hao123\.com|loco\.yahoo\.co\.jp|paconet\.starthome\.jp)$;ga:source!~(s\.cube-soft\.jp|search\.myjcom\.jp|so-net\.ne\.jp|accountpage\.line\.me|search\.dressapp\.aswidget\.com)$;ga:source!~(search\.jiqoo\.jp|www2\.delta-search\.com|r\.duckduckgo\.com|search\.livedoor\.com|smaken\.jp)$";

    /**
     * ランディングページ以外へのアクセス
     */
    const InNotLP = "ga:pagePath!~(/|_)(lp\d*/|campaign\d*/)";

    /**
     * 広告からの流入
     */
    const FromAd = "ga:medium=~^(cpc|ppc|cpa|cpm|cpv|cpp)$";

    /**
     * 広告以外からの流入
     */
    const FromNotAd = "ga:medium!~^(cpc|ppc|cpa|cpm|cpv|cpp)$";

    /**
     * SNSからの流入
     */
    const FromSNS = "ga:source=~(facebook\.com|^matome\.naver\.jp|^t\.co|^mixi\.jp|^pinterest\.com|instagram\.com|^youtube\.com)$";

    /**
     * SNS以外からの流入
     */
    const FromNotSNS = "ga:source!~(facebook\.com|^matome\.naver\.jp|^t\.co|^mixi\.jp|^pinterest\.com|^instagram\.com|^youtube\.com)$";

    /**
     * Facebookからの流入
     */
    const FromFacebook = "ga:source=~facebook\.com$";

    /**
     * Facebook以外からの流入
     */
    const FromNotFacebook = "ga:source!~facebook\.com$";

    /**
     * ブログからの流入
     */
    const FromBlog = "ga:source=~(blog\.livedoor\.jp|(s\.)?ameblo\.jp|blogs\.yahoo\.co\.jp|\.fc2\.com|\.jugem\.jp)$";

    /**
     * ブログ以外からの流入
     */
    const FromNotBlog = "ga:source!~(blog\.livedoor\.jp|(s\.)?ameblo\.jp|blogs\.yahoo\.co\.jp|\.fc2\.com|\.jugem\.jp)$";

    /**
     * CallCV除外
     */
    const AntiCallCV = "ga:pagePath!~/callcv";

    /**
     * 検索用ハッシュマップ
     */
    private $filters = [
        "direct"                   => self::Direct,
        "not_direct"               => self::NotDirect,
        "from_organic_search"      => self::FromOrganicSearch,
        "from_not_organic_search"  => self::FromNotOrganicSearch,
        "in_not_lp"                => self::InNotLP,
        "from_ad"                  => self::FromAd,
        "from_not_ad"              => self::FromNotAd,
        "from_sns"                 => self::FromSNS,
        "from_not_sns"             => self::FromNotSNS,
        "from_facebook"            => self::FromFacebook,
        "from_not_facebook"        => self::FromNotFacebook,
        "from_blog"                => self::FromBlog,
        "from_not_blog"            => self::FromNotBlog,
        "anti_callcv"              => self::AntiCallCV,
        "not_callcv"               => self::AntiCallCV,
    ];

    /**
     * データ取得用
     *
     * @throws if 要求されたフィルタ名が見つからない場合
     */
    public function __get($name)
    {
        if (isset($this->filters[$name])) {
            return $this->filters[$name];
        } else {
            throw new \Exception("Filters '$name' not found in Filters Library.");
        }
    }
}
