<?php

namespace StereotypeUI\Action;


class DefaultAction
{
    private $response;


    public function __construct($response)
    {
        $this->response = $response;
    }

    public function __invoke($request, $response)
    {
        $path = $request->getUri()->getPath();

        if (!preg_match("%^/api/%", $path)) {
          $index = file_get_contents("index.html");

          return $this->response->write($index);
        }

        return $this->response
            ->withJson([
                "error" => [
                    "id" => "unknown_api",
                    "message" => "Request to unknown API path: ${path}",
                ],
            ], 404, JSON_PRETTY_PRINT);
    }
}
