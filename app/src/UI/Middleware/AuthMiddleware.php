<?php

namespace StereotypeUI\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

use Stereotype\Service\Client;


class AuthMiddleware
{
    public function __construct($client)
    {
        $this->client = $client;
    }

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface      $response,
        callable               $next
    )
    {
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
        }

        if ($this->is_logged_in()) {
            if ("/logout" === $request->getUri()->getPath()) {
                unset($_SESSION['access_token']);

                if(isset($_COOKIE[session_name()])){
                  setcookie(session_name(),'',time()-86400,'/');
                }
                session_destroy();

                return $response->withStatus(302)
                    ->withHeader("Location", "/auth");
            }

            return $next($request, $response);
        }


        if ("/auth" !== $request->getUri()->getPath()) {
            return $response->withStatus(302)
                ->withHeader("Location", "/auth");
        } else {
            $params = $request->getQueryParams();

            if (!isset($params["code"])) {
                $auth_url = $this->client->createAuthUrl();

                return $response->withStatus(302)
                    ->withHeader("Location", $auth_url);
            } else {
                $this->client->authenticate($params["code"]);

                return $response->withStatus(302)
                    ->withHeader("Location", "/");
            }
        }
    }

    private function is_logged_in()
    {
        return !$this->client->isAccessTokenExpired();
    }
}
