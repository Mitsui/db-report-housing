<?php

namespace Stereotype\Core;

use Carbon\Carbon;

use PHPExcel;
use PHPExcel_IOFactory;

use Google_Service_Analytics_GaData;
use Google_Service_Analytics_GaDataProfileInfo;
use Google_Service_Analytics_GaDataQuery;

// use Stereotype\Core\Blueprint;

use Stereotype\Util\Processer;


class Summary
{
    /**
     * サマリーが作られた日時
     *
     * @var \Carbon\Carbon
     */
    private $created_at;

    /**
     * GaData 実体の配列
     *
     * @var array
     */
    private $rows;

    /**
     * 対象アカウントの情報
     *
     * @var \Google_Service_Analytics_GaDataProfileInfo
     */
    private $profile_info;

    /**
    * 問い合わせ内容
    *
    * @var \Google_Service_Analytics_GaDataQuery
    */
    private $query;


    // public function __construct(
    //     $name,
    //     Google_Service_Analytics_GaData $data
    // )
    public function __construct()
    {
        $this->created_at = Carbon::now();

        $this->rows = [];

        $this->profile_info = null;

        $this->query = null;
    }


    /**
     * GaData データを追加
     *
     * @var string                           $category カテゴリー名
     * @var \Google_Service_Analytics_GaData $ga_data
     * @return self
     */
    public function add(
        $category,
        Google_Service_Analytics_GaData $data
    )
    {
        if (is_null($this->profile_info)) {
            $this->setProfileInfo($data->getProfileInfo());
        }

        if (is_null($this->query)) {
            $this->setQuery($data->getQuery());
        }


        $rows = $data->getRows();
        if (is_null($rows)) { $rows = []; }

        $column_names = array_map(function($header) {
            return $header->getName();
        }, $data->getColumnHeaders());

        $rows = self::setNamedKeys($column_names, $rows);

        $rows = self::addCategoryColumn($category, $rows);

        $rows = self::defaultProcess($rows);

        $this->setRows(array_merge($this->getRows(), $rows));


        return $this;
    }


    /**
     * 各行の key を column_names に貼り替える
     *
     * @var    array　$column_names 貼り替えるカラム名の配列
     * @var    array　$rows         keyを貼り替えられるデータ
     * @return array
     */
    private static function setNamedKeys(array $column_names, array $rows)
    {
        return array_map(function($row) use ($column_names) {
            return array_combine($column_names, $row);
        }, $rows);
    }

    /**
     * 各行に category カラムを追加
     *
     * @var    array
     * @return array
     */
    private static function addCategoryColumn($category, array $rows)
    {
        return array_map(function($row) use ($category){
            return array_merge(
                ["category" => $category],
                $row
            );
        }, $rows);
    }

    /**
     * 一律のデータ処理を適用
     *
     * @var    array $rows
     * @return array
     */
    private static function defaultProcess(array $rows)
    {
        $processed = [];

        foreach ($rows as $row) {
            $processed_row = [];

            foreach ($row as $label => $data) {
                switch ($label) {
                    case "ga:yearMonth":
                        $processed_row[$label] = Processer::year_month($data);
                        break;

                    case "ga:hour":
                        $processed_row[$label] = Processer::hour($data);
                        break;

                    case "ga:bounceRate":
                    case "ga:exitRate":
                    case "ga:goalConversionRateAll":
                        $processed_row[$label] = Processer::percent($data);
                        break;

                    case "ga:dayOfWeek":
                        $processed_row[$label] = Processer::ja_dow($data);
                        break;

                    default:
                        $processed_row[$label] = $data;
                }
            }

            $processed[] = $processed_row;
        }

        return $processed;
    }


    /**
     * 最初の行のカラム名を返す
     *
     * @return array
     */
    public function getColumnNames()
    {
        $rows = $this->getRows();

        if (empty($rows)) {
            return [];
        } else {
            return array_keys(array_shift($rows));
        }
    }

    /**
     * 集計開始日を取得
     *
     * @return \Carbon\Carbon
     */
    public function getStartDate()
    {
        $start_date = $this->getQuery()->getStartDate();

        return new Carbon($start_date);
    }

    /**
     * 集計終了日を取得
     *
     * @return \Carbon\Carbon
     */
    public function getEndDate()
    {
        $end_date = $this->getQuery()->getEndDate();

        return new Carbon($end_date);
    }

    /**
     * setter: $rows
     *
     * @var    array $rows
     * @return self
     */
    private function setRows($rows)
    {
        $this->rows = $rows;

        return $this;
    }

    /**
     * getter: $rows
     *
     * @return array
     */
    public function getRows()
    {
        return $this->rows;
    }


    /**
     * setter: $profile_info
     *
     * @var    \Google_Service_Analytics_GaDataProfileInfo $profile_info
     * @return self
     */
    private function setProfileInfo(Google_Service_Analytics_GaDataProfileInfo $profile_info)
    {
        $this->profile_info = $profile_info;

        return $this;
    }

    /**
     * getter: $profile_info
     *
     * @return \Google_Service_Analytics_GaDataProfileInfo
     */
    private function getProfileInfo()
    {
        return $this->profile_info;
    }


    /**
     * setter: $query
     *
     * @var    \Google_Service_Analytics_GaDataQuery $query
     * @return self
     */
    private function setQuery(Google_Service_Analytics_GaDataQuery $query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * getter: $query
     *
     * @return \Google_Service_Analytics_GaDataQuery
     */
    private function getQuery()
    {
        return $this->query;
    }


    /**
     * アカウントID を取得
     *
     * @return string
     */
    public function getAccountId()
    {
        return $this->getProfileInfo()->getWebPropertyId();
    }

    /**
     * ビューID (Profile ID) を取得
     *
     * @return string
     */
    public function getViewId()
    {
        return $this->getProfileInfo()->getTableId();
    }

    /**
     * サマリーの総合的な情報を返す。表示用。
     *
     * @return array
     */
    private function getInfo()
    {
        $created_at = $this->created_at->format("Y/m/d H:i:s");
        $accout_id = $this->getAccountId();
        $view_id = $this->getViewId();
        $start_date = $this->getStartDate()->format("Y/m/d");
        $end_date = $this->getEndDate()->format("Y/m/d");

        return [
            ["集計データ取得日時:", $created_at],
            ["アカウントID:", $accout_id],
            ["ビューID:"   , $view_id  ],
            ["集計期間:"      , $start_date, "~", $end_date],
        ];
    }

    /**
     * データの実体をCSV形式の文字列で取得
     *
     * @var    bool $header_row CSVの1行目にヘッダー列を追加するかどうか
     * @return string
     */
    public function toCSV($info= true, $header_row = true)
    {
        $data_info = $this->getInfo();
        $column_names = $this->getColumnNames();
        $rows = $this->getRows();

        $csv = array_merge(
            $data_info,
            [[""]],
            [$column_names],
            $rows
        );

        return $csv;
    }

    /**
     * データの実体をCSV形式で保存する
     *
     * @var    string $filepath 保存先の Path
     * @return self
     */
    public function saveCSV($filepath)
    {
        $book = new PHPExcel();
        $sheet = $book->getSheet(0);

        // Data Info
        $sheet->fromArray($this->getInfo(), null, "A1");

        // Column Header
        $sheet->fromArray($this->getColumnNames(), null, "A5");

        // Datas
        $sheet->fromArray($this->getRows(), null, "A6");

        $writer = PHPExcel_IOFactory::createWriter($book, "CSV");
        $writer->save($filepath);

        return $this;
    }

    /**
     * コールバック関数によってデータを加工
     *
     * @var    callable $callback データの各行を加工するコールバック関数
     * @return self
     */
    public function process(callable $callback)
    {
        $rows = $this->getRows();

        $processed = [];

        foreach ($rows as $row) {
            $processed_row = $callback($row, $rows);

            if (!is_null($processed_row) && !empty($processed_row)) {
                $processed[] = $processed_row;
            }
        }

        $this->setRows($processed);

        return $this;
    }
}
