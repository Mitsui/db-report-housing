<?php

namespace Stereotype\Core;


class Query
{
    /**
     * ビューID
     *
     * @var string
     */
    private $view_id;

    /**
     * 集計開始日
     *
     * @var \Carbon\Carbon
     */
    private $start_date;

    /**
     * 集計終了日
     *
     * @var \Carbon\Carbon
     */
    private $end_date;

    /**
     * 指標
     *
     * @var array
     */
    private $metrics;

    /**
     * ディメンション
     *
     * @var array
     */
    private $dimensions;

    /**
     * 集計結果の並び順
     *
     * @var array
     */
    private $sort;

    /**
     * フィルター
     *
     * @var string
     */
    private $filters;

    /**
     * セグメント
     *
     * @var string
     */
    private $segment;

    /**
     * 集計結果取得数
     *
     * @var int|null
     */
    private $max_results;


    public function __construct($args)
    {
        extract($args);

        $this->view_id     = $view_id;
        $this->start_date  = $start_date;
        $this->end_date    = $end_date;
        $this->metrics     = $metrics;
        $this->dimensions  = $dimensions;
        $this->sort        = $sort;
        $this->filters     = isset($filters) ? $filters : "";
        $this->segment     = isset($segment) ? $segment : "";
        $this->max_results = $max_results;
    }

    public function __toString()
    {
        $start_date  = $this->getStartDate();
        $end_date    = $this->getEndDate();
        $metrics     = $this->getMetrics();
        $dimensions  = $this->getDimensions();
        $sort        = $this->getSort();
        $filters     = $this->getFilters();
        $segment     = $this->getSegment();
        $max_results = $this->getMaxResults();

        return <<< EOD
start_date : $start_date
end_date   : $end_date
metrics    : $metrics
dimensions : $dimensions
sort       : $sort
filters    : $filters
segment    : $segment
max_results: $max_results

EOD;
    }

    public function __call($method, $args)
    {
        if($method === 'clone') {
            return call_user_func_array(array($this, '_clone'), $args);
        } else {
            throw new \LogicException("Unknown method: $method");
        }
    }

    public function _clone($args)
    {
        extract($args);

        $cloned = clone $this;

        if (isset($filters)) {
            $cloned->setFilters($filters);
        }

        if (isset($segment)) {
            $cloned->setSegment($segment);
        }

        return $cloned;
    }

    /**
     * @return string
     */
    public function getViewId()
    {
        return $this->view_id;
    }


    /**
     * @return \Carbon\Carbon
     */
    public function getStartDate()
    {
        return $this->start_date;
    }


    /**
     * @return \Carbon\Carbon
     */
    public function getEndDate()
    {
        return $this->end_date;
    }


    /**
     * @return array
     */
    public function getMetrics()
    {
        return $this->metrics;
    }


    /**
     * @return array
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @return bool
     */
    public function hasDimensions()
    {
        return !empty($this->dimensions);
    }


    /**
     * @return array
     */
    public function getSort()
    {
        return $this->sort;
    }


    /**
     * @return bool
     */
    public function hasSort()
    {
        return !empty($this->sort);
    }


    /**
     * @var    string $filters
     * @return self
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @return bool
     */
    public function hasFilters()
    {
        return !empty($this->filters);
    }


    /**
     * @var    string $segment
     * @return self
     */
    public function setSegment($segment)
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * @return string
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * @return bool
     */
    public function hasSegment()
    {
        return !empty($this->segment);
    }


    /**
     * @return bool
     */
    public function hasMaxResults()
    {
        return !is_null($this->max_results);
    }

    /**
     * @return int
     */
    public function getMaxResults()
    {
        return $this->max_results;
    }
}
