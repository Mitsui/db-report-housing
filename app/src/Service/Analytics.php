<?php

namespace Stereotype\Service;

use Carbon\Carbon;

use Google_Client;
use Google_Service_Analytics;


class Analytics extends Google_Service_Analytics
{
    /**
     * Google Analytics 集計リクエストを生成
     *
     * @return \Google_Http_Request
     */
    public function get(
        $view_id,
        Carbon $start_date,
        Carbon $end_date,
        array $metrics,
        array $options
    )
    {
        return $this->data_ga->get(
            "ga:".$view_id,
            $start_date->toDateString(),
            $end_date->toDateString(),
            join(",", $metrics),
            $options
        );
    }


    /**
     * アカウント一覧を取得
     *
     * @return array(Google_Service_Analytics_Account)
     */
    public function getAccounts()
    {
        return $this->management_accounts
                   ->listManagementAccounts()
                   ->getItems();
    }

    /**
     * プロパティ一覧を取得
     *
     * @var    string $account_id プロパティ一覧を取得するアカウントのID
     *
     * @return array(Google_Service_Analytics_Webproperty)
     */
    public function getProperties($account_id)
    {
        return $this->management_webproperties
                   ->listManagementWebproperties($account_id)
                   ->getItems();
    }

    /**
     * ビュー一覧を取得
     *
     * @var    string $account_id  ビュー一覧を取得するアカウントのID
     * @var    string $property_id ビュー一覧を取得するプロパティのID
     *
     * @return array(Google_Service_Analytics_Webproperty)
     */
    public function getViews($account_id, $property_id)
    {
        return $this->management_profiles
                   ->listManagementProfiles($account_id, $property_id)
                   ->getItems();
    }
}
