<?php

namespace Stereotype\DB;

use ReflectionClass;


class Site extends Main
{
    /**
     * DBにフィルターが登録されていなかったときに使われるデフォルトのフィルター
     */
    // トップページ
    const ALL_TOP_FILTERS = "ga:pagePath=~^/($|\?),ga:pagePath=~^/sp/($|\?)";
    const PC_TOP_FILTERS  = "ga:pagePath=~^/($|\?)";
    const SP_TOP_FILTERS  = "ga:pagePath=~^/sp/($|\?)";

    // フェア一覧
    const ALL_EVENT_LIST_FILTERS = "ga:pagePath=~/event/;ga:pagePath!~/event/detail";
    const PC_EVENT_LIST_FILTERS  = "ga:pagePath=~^/event/;ga:pagePath!~^/event/detail";
    const SP_EVENT_LIST_FILTERS  = "ga:pagePath=~^/sp/event/;ga:pagePath!~^/sp/event/detail";

    // フェア詳細
    const ALL_EVENT_DETAIL_FILTERS = "ga:pagePath=~/event/detail";
    const PC_EVENT_DETAIL_FILTERS  = "ga:pagePath=~^/event/detail";
    const SP_EVENT_DETAIL_FILTERS  = "ga:pagePath=~^/sp/event/detail";

    // フェア予約 入力画面
    const ALL_EVENT_INDEX_FILTERS = "ga:pagePath=~/reserve_f/\?;ga:pagePath!~/reserve_f/.*confirm";
    const PC_EVENT_INDEX_FILTERS  = "ga:pagePath=~^/reserve_f/\?;ga:pagePath!~^/reserve_f/.*confirm";
    const SP_EVENT_INDEX_FILTERS  = "ga:pagePath=~^/sp/reserve_f/\?;ga:pagePath!~^/sp/reserve_f/.*confirm";

    // フェア予約 確認画面
    const ALL_EVENT_CONFIRM_FILTERS = "ga:pagePath=~/reserve_f/.*confirm";
    const PC_EVENT_CONFIRM_FILTERS  = "ga:pagePath=~^/reserve_f/.*confirm";
    const SP_EVENT_CONFIRM_FILTERS  = "ga:pagePath=~^/sp/reserve_f/.*confirm";

    // フェア予約 完了画面
    const ALL_EVENT_THANKS_FILTERS = "ga:pagePath=~/reserve_f/thanks";
    const PC_EVENT_THANKS_FILTERS  = "ga:pagePath=~^/reserve_f/thanks";
    const SP_EVENT_THANKS_FILTERS  = "ga:pagePath=~^/sp/reserve_f/thanks";

    // 来館予約 入力画面
    const ALL_RESERVE_INDEX_FILTERS = "ga:pagePath=~/reserve/$";
    const PC_RESERVE_INDEX_FILTERS  = "ga:pagePath=~^/reserve/$";
    const SP_RESERVE_INDEX_FILTERS  = "ga:pagePath=~^/sp/reserve/$";

    // 来館予約 確認画面
    const ALL_RESERVE_CONFIRM_FILTERS = "ga:pagePath=~/reserve/.*confirm";
    const PC_RESERVE_CONFIRM_FILTERS  = "ga:pagePath=~^/reserve/.*confirm";
    const SP_RESERVE_CONFIRM_FILTERS  = "ga:pagePath=~^/sp/reserve/.*confirm";

    // 来館予約 完了画面
    const ALL_RESERVE_THANKS_FILTERS = "ga:pagePath=~/reserve/thanks";
    const PC_RESERVE_THANKS_FILTERS  = "ga:pagePath=~^/reserve/thanks";
    const SP_RESERVE_THANKS_FILTERS  = "ga:pagePath=~^/sp/reserve/thanks";

    // 資料請求 入力画面
    const ALL_INQUIRY_INDEX_FILTERS = "ga:pagePath=~/inquiry/$";
    const PC_INQUIRY_INDEX_FILTERS  = "ga:pagePath=~^/inquiry/$";
    const SP_INQUIRY_INDEX_FILTERS  = "ga:pagePath=~^/sp/inquiry/$";

    // 資料請求 確認画面
    const ALL_INQUIRY_CONFIRM_FILTERS = "ga:pagePath=~/inquiry/.*confirm";
    const PC_INQUIRY_CONFIRM_FILTERS  = "ga:pagePath=~^/inquiry/.*confirm";
    const SP_INQUIRY_CONFIRM_FILTERS  = "ga:pagePath=~^/sp/inquiry/.*confirm";

    // 資料請求 完了画面
    const ALL_INQUIRY_THANKS_FILTERS = "ga:pagePath=~/inquiry/thanks";
    const PC_INQUIRY_THANKS_FILTERS  = "ga:pagePath=~^/inquiry/thanks";
    const SP_INQUIRY_THANKS_FILTERS  = "ga:pagePath=~^/sp/inquiry/thanks";

    // お問い合わせ 入力画面
    const ALL_CONTACT_INDEX_FILTERS = "ga:pagePath=~/contact/$";
    const PC_CONTACT_INDEX_FILTERS  = "ga:pagePath=~^/contact/$";
    const SP_CONTACT_INDEX_FILTERS  = "ga:pagePath=~^/sp/contact/$";

    // お問い合わせ 確認画面
    const ALL_CONTACT_CONFIRM_FILTERS = "ga:pagePath=~/contact/.*confirm";
    const PC_CONTACT_CONFIRM_FILTERS  = "ga:pagePath=~^/contact/.*confirm";
    const SP_CONTACT_CONFIRM_FILTERS  = "ga:pagePath=~^/sp/contact/.*confirm";

    // お問い合わせ 完了画面
    const ALL_CONTACT_THANKS_FILTERS = "ga:pagePath=~/contact/thanks";
    const PC_CONTACT_THANKS_FILTERS  = "ga:pagePath=~^/contact/thanks";
    const SP_CONTACT_THANKS_FILTERS  = "ga:pagePath=~^/sp/contact/thanks";

    // フォーム以外の全てのページ
    const NOT_FORM_FILTERS  = "ga:pagePath!~(/reserve/|/reserve_f/|/inquiry/|/contact/)";


    const ANYTHING_BUT_FILTERS = "ga:deviceCategory==anythingbut";


    const SPECIFIC_SITE_FILTERS     = "ga:source==anythingbut";
    const NOT_SPECIFIC_SITE_FILTERS = "ga:deviceCategory=~desktop|tablet|mobile";


    // PCサイトを閲覧
    const VIEW_PC_SITE_SEGMENT  = "sessions::condition::!ga:pagePath=~/sp/";
    // SPサイトを閲覧
    const VIEW_SP_SITE_SEGMENT  = "sessions::condition::ga:pagePath=~/sp/";


    const ANYTHING_BUT_SEGMENT = "sessions::condition::ga:deviceCategory==anythingbut";



    /**
     * ビューID
     *
     * @var string
     */
    private $view_id;


    public function __construct($view_id)
    {
        $this->view_id = $view_id;

        parent::__construct();
    }

    /**
     * getter: $view_id
     *
     * @return string
     */
    public function getViewId()
    {
        return $this->view_id;
    }

    /**
     * キーワードをもとにビューIDを検索
     *
     * @var    string      $keyword
     * @return string|null
     */
    public static function findViewId($keyword)
    {
        $db = new Main;

        if (preg_match("/^UA-(\d+)-\d+$/", $keyword, $m)) {
            $account_id = $m[1];

            $stmt = $db->prepare("
                SELECT `view_id` FROM `sites`
                WHERE `account_id` = ?
            ");

            $stmt->execute([
                $account_id,
            ]);
            $results = $stmt->fetchAll();

            if (!empty($results)) { return $results[0]["view_id"]; }
        } else if (preg_match("/^\d+$/", $keyword)) {
            $stmt = $db->prepare("
                SELECT `view_id` FROM `sites`
                WHERE `account_id` = ?
            ");

            $stmt->execute([
                $keyword,
            ]);
            $results = $stmt->fetchAll();

            if (!empty($results)) { return $results[0]["view_id"]; }


            $stmt = $db->prepare("
                SELECT count(*) FROM `sites`
                WHERE `view_id` = ?
            ");

            $stmt->execute([
                $keyword,
            ]);
            $results = $stmt->fetchAll();

            if (!empty($results) && $results[0]["count(*)"] > 0) {
                return $keyword;
            }
        } else if (mb_strlen($keyword) > 5) {
            $stmt = $db->prepare("
                SELECT `view_id` FROM `sites`
                WHERE `url` LIKE ?
            ");

            $stmt->execute([
                "%${keyword}%",
            ]);
            $results = $stmt->fetchAll();

            if (!empty($results)) { return $results[0]["view_id"]; }
        }

        return null;
    }

    /**
     * アカウントIDをもとにオブジェクトを生成
     *
     * @return self
     */
    public static function createFromAccountId($account_id)
    {
        $db = new Main;

        $stmt = $db->prepare("
            SELECT `view_id` FROM `sites`
            WHERE `account_id` = ?
        ");

        $stmt->execute([
            $account_id,
        ]);
        $results = $stmt->fetchAll();

        if (empty($results)) { return null; }

        $view_id = $results[0]["view_id"];

        return new self($view_id);
    }

    /**
     * URLをもとにオブジェクトを生成
     *
     * @return self
     */
    public static function createFromUrl($url)
    {
        $db = new Main;

        $stmt = $db->prepare("
            SELECT `view_id` FROM `sites`
            WHERE `url` LIKE ?
        ");

        $stmt->execute([
            "%${url}%",
        ]);
        $results = $stmt->fetchAll();

        if (empty($results)) { return null; }

        $view_ids = array_map(function($row) {
            return $row["view_id"];
        }, $results);
        $site = new self($view_ids[0]);

        return [
            "site"     => $site,
            "results"  => count($view_ids),
            "view_ids" => $view_ids,
        ];
    }

    /**
     * 不明なキーワードをもとに頑張ってオブジェクトを生成
     *
     * @var    string $keyword
     * @return self
     */
    public static function createFromKeyword($keyword)
    {
        $view_id = self::findViewId($keyword);

        if (!is_null($view_id)) {
            return new self($view_id);
        } else {
            return null;
        }
    }


    /**
     * サイト名を返す
     *
     * @return string
     */
    public function getName()
    {
        if (!isset($this->stmt_get_name)) {
            $this->stmt_get_name = $this->prepare("
                SELECT `name` FROM `sites`
                WHERE `view_id` = ?
            ");
        }

        $stmt = $this->stmt_get_name;

        $stmt->execute([
            $this->view_id,
        ]);
        $results = $stmt->fetchAll();

        if (empty($results)) { return null; }

        return $results[0]["name"];
    }

    /**
     * アカウントIDを取得
     *
     * @return string
     */
    public function getAccountId()
    {
        if (!isset($this->stmt_get_account_id)) {
            $this->stmt_get_account_id = $this->prepare("
                SELECT `account_id` FROM `sites`
                WHERE `view_id` = ?
            ");
        }

        $stmt = $this->stmt_get_account_id;

        $stmt->execute([
            $this->view_id,
        ]);
        $results = $stmt->fetchAll();

        if (empty($results)) { return null; }

        return $results[0]["account_id"];
    }


    /**
     * DBからフィルターの取得を全て引き受ける
     */
    public function __get($name)
    {
        if (preg_match("/([0-9a-z_]+)_filters/", $name, $m)) {
            return $this->getFilters($m[1]);
        }

        if (preg_match("/([0-9a-z_]+)_segment/", $name, $m)) {
            return $this->getSegment($m[1]);
        }

        $ref = new ReflectionClass($this);
        $class_name = $ref->getName();

        throw new \Exception(
            "Undefined property: ${class_name}::$${name}"
        );
    }

    /**
     * ページパス選別用のフィルターを返す
     *
     * @var    srting $page デバイス、フォーム名、ページなどを指定する文字列
     * @return string|null
     */
    public function getFilters($page)
    {
        if (!isset($this->stmt_get_filters)) {
            $this->stmt_get_filters = $this->prepare("
                SELECT `filters`
                FROM `sites` INNER JOIN `page_filters`
                ON `sites`.`id` = `page_filters`.`site_id`
                WHERE `view_id` = ? AND `page` = ?
            ");
        }

        $stmt = $this->stmt_get_filters;

        $stmt->execute([
            $this->view_id,
            $page,
        ]);
        $results = $stmt->fetchAll();

        if (empty($results)) {
            $default_filter = "self::".strtoupper($page)."_FILTERS";

            return constant($default_filter);
        } else if (is_null($results[0]["filters"])) {
            return self::ANYTHING_BUT_FILTERS;
        } else {
            return $results[0]["filters"];
        }
    }

    /**
     * PCサイト/SPサイト 選別用のセグメントを返す
     *
     * @var    srting $name デバイスを指定する文字列
     * @return string|null
     */
    public function getSegment($name)
    {
        if (!isset($this->stmt_get_segment)) {
            $this->stmt_get_segment = $this->prepare("
                SELECT `site_segment`.`segment`
                FROM `sites` INNER JOIN `site_segment`
                ON `sites`.`id` = `site_segment`.`site_id`
                WHERE `sites`.`view_id` = ? AND `site_segment`.`name` = ?
            ");
        }

        $stmt = $this->stmt_get_segment;

        $stmt->execute([
            $this->view_id,
            $name,
        ]);
        $results = $stmt->fetchAll();

        if (empty($results)) {
            $default_segment = "self::".strtoupper($name)."_SEGMENT";

            return constant($default_segment);
        } else if (is_null($results[0]["segment"])) {
            return self::ANYTHING_BUT_SEGMENT;
        } else {
            return $results[0]["segment"];
        }
    }
}
