<?php

namespace Stereotype\DB;

use Carbon\Carbon;


class Calendar extends Main
{
    /**
     * 祝日判定
     *
     * @var \Carbon\Carbon $date
     * @return bool
     */
    public function isHoliday(Carbon $date)
    {
        if (!isset($this->stmt_is_holiday)) {
            $this->stmt_is_holiday = $this->prepare("
                SELECT `id` FROM `holidays`
                WHERE `deleted_at` IS NULL AND `date` = ?
                LIMIT 1
            ");
        }

        $stmt = $this->stmt_is_holiday;

        $stmt->execute([
            $date->format("Y-m-d"),
        ]);
        $results = $stmt->fetchAll();

        return !empty($results);
    }

    /**
     * 祝日の日本語名を取得
     *
     * @var \Carbon\Carbon $date
     * @return string|null
     */
    public function holidaysName(Carbon $date)
    {
        if (!isset($this->stmt_holidays_name)) {
            $this->stmt_holidays_name = $this->prepare("
                SELECT `name` FROM `holidays`
                WHERE `deleted_at` IS NULL AND `date` = ?
                LIMIT 1
            ");
        }

        $stmt = $this->stmt_holidays_name;

        $stmt->execute([
            $date->format("Y-m-d"),
        ]);
        $results = $stmt->fetchAll();

        // 指定された日が祝日でない場合は null を返す
        if (empty($results)) { return null; }

        return $results[0]["name"];
    }
}
