<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class MonthlyConversionFromAd extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subYears(2)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:users",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [
            "ga:yearMonth",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return array_filter([
            "PC+SP フェア予約" => $site->all_event_thanks_filters,
            "PC+SP 来館予約" => $site->all_reserve_thanks_filters,
            "PC+SP 資料請求" => $site->all_inquiry_thanks_filters,
            "PC+SP お問い合わせ" => $site->all_contact_thanks_filters,

            "PC フェア予約" => $site->pc_event_thanks_filters,
            "PC 来館予約" => $site->pc_reserve_thanks_filters,
            "PC 資料請求" => $site->pc_inquiry_thanks_filters,
            "PC お問い合わせ" => $site->pc_contact_thanks_filters,

            "SP フェア予約" => $site->sp_event_thanks_filters,
            "SP 来館予約" => $site->sp_reserve_thanks_filters,
            "SP 資料請求" => $site->sp_inquiry_thanks_filters,
            "SP お問い合わせ" => $site->sp_contact_thanks_filters,
        ]);
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $lib->not_spam.";".$lib->from_ad;
    }
}
