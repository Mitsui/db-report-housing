<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class PercentContributionRanking extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subMonths(2)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:uniquePageviews",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [
            "ga:pagePath",
        ];
    }

    /**
     * 集計結果の並び順のデフォルト
     *
     * @return array
     */
    public function sort()
    {
        return [
            "-ga:uniquePageviews",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $site->not_form_filters;
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return [
            "コンバージョンしたユーザー" => $lib->not_spam.";".$lib->converters,
            "全てのユーザー" => $lib->not_spam,
        ];
    }

    /**
     * 集計後に実行されるコールバック
     *
     * @var    array $row  集計結果の1行
     * @var    array $rows 集計結果全体
     * @return array|null
     */
    public function callback(array $row, array $rows)
    {
        // 「コンバージョンしたユーザーからのPV数」と「すべてのユーザーからのPV数」から
        // コンバージョン寄与率を計算する
        // 「コンバージョン寄与率」=「コンバージョンしたユーザーからのPV数」/「すべてのユーザーからのPV数」

        // コンバージョンしたユーザーの数字のみ結果に残す
        if ("コンバージョンしたユーザー" !== $row["category"]) {
            return null;
        }

        $path = $row["ga:pagePath"];
        $converters_pv = $row["ga:uniquePageviews"];

        $allusers_pv = self::searchPageview($path, $rows);

        if (is_null($allusers_pv) || $allusers_pv <= 100) {
            // 全てのユーザーのPV数が100を下回るページは
            // 寄与率の信頼性に欠けるので捨てる
            return null;
        } else {
            return [
                "URL" => $path,
                "PC/SP" => preg_match("%^/sp/%", $path) ? "SP" : "PC",
                "PV数(コンバージョンしたユーザー)" => (string)$converters_pv,
                "PV数(全てのユーザー)" => (string)$allusers_pv,
                "コンバージョン寄与率" => (string)($converters_pv / $allusers_pv),
            ];
        }
    }

    public static function searchPageview($path, array $rows) {
        foreach ($rows as $row) {
            if ("コンバージョンしたユーザー" !== $row["category"] && $path === $row["ga:pagePath"]) {
                return $row["ga:uniquePageviews"];
            }
        }

        return null;
    }
}
