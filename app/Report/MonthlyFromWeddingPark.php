<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class MonthlyFromWeddingPark extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subYears(2)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:users",
            "ga:uniquePageviews",
        ];
    }

    /**
     * ディメンション
     *
     * @return array
     */
    public function dimensions()
    {
        return [
            "ga:yearMonth",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return [
            "流入数" => "",
            "PC_フェア予約コンバージョン数" => $site->pc_event_thanks_filters,
            "PC_来館予約コンバージョン数" => $site->pc_reserve_thanks_filters,
            "PC_資料請求コンバージョン数" => $site->pc_inquiry_thanks_filters,
            "PC_お問い合わせコンバージョン数" => $site->pc_contact_thanks_filters,
            "SP_フェア予約コンバージョン数" => $site->sp_event_thanks_filters,
            "SP_来館予約コンバージョン数" => $site->sp_reserve_thanks_filters,
            "SP_資料請求コンバージョン数" => $site->sp_inquiry_thanks_filters,
            "SP_お問い合わせコンバージョン数" => $site->sp_contact_thanks_filters,
        ];
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $lib->not_spam.";".$lib->from_wedding_park;
    }
}
