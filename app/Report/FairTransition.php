<?php

namespace Stereotype\Report;

use Carbon\Carbon;

use Stereotype\Core\Blueprint;

use Stereotype\DB\Calendar;
use Stereotype\DB\Site;

use Stereotype\Library\Segment as SegmentLib;
use Stereotype\Library\Filters as FiltersLib;


class FairTransition extends Blueprint
{
    /**
     * 集計開始日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function startDate() {
        return Carbon::now()->subMonths(6)->startOfMonth();
    }

    /**
     * 集計終了日(必須)
     *
     * @return \Carbon\Carbon
     */
    public function endDate() {
        return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth();
    }

    /**
     * 指標(必須)
     *
     * @return array
     */
    public function metrics()
    {
        return [
            "ga:users",
            "ga:uniquePageviews",
            "ga:exitRate",
        ];
    }

    /**
     * フィルター
     *
     * @var    FiltersLib $lib      フィルターライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function filters(
        FiltersLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return [
            "PC+SP トップページ" => $site->all_top_filters,
            "PC+SP フェア一覧" => $site->all_fair_list_filters,
            "PC+SP フェア詳細" => $site->all_fair_detail_filters,
            "PC+SP フェア予約 入力画面" => $site->all_fair_index_filters,
            "PC+SP フェア予約 確認画面" => $site->all_fair_confirm_filters,
            "PC+SP フェア予約 完了画面" => $site->all_fair_thanks_filters,

            "PC トップページ" => $site->pc_top_filters,
            "PC フェア一覧" => $site->pc_fair_list_filters,
            "PC フェア詳細" => $site->pc_fair_detail_filters,
            "PC フェア予約 入力画面" => $site->pc_fair_index_filters,
            "PC フェア予約 確認画面" => $site->pc_fair_confirm_filters,
            "PC フェア予約 完了画面" => $site->pc_fair_thanks_filters,

            "SP トップページ" => $site->sp_top_filters,
            "SP フェア一覧" => $site->sp_fair_list_filters,
            "SP フェア詳細" => $site->sp_fair_detail_filters,
            "SP フェア予約 入力画面" => $site->sp_fair_index_filters,
            "SP フェア予約 確認画面" => $site->sp_fair_confirm_filters,
            "SP フェア予約 完了画面" => $site->sp_fair_thanks_filters,
        ];
    }

    /**
     * セグメント
     *
     * @var    SegmentLib $lib      セグメントライブラリ
     * @var    Calendar   $calendar カレンダーDB
     * @var    Site       $site     サイトDB
     * @return string|array
     */
    public function segment(
        SegmentLib $lib,
        Calendar   $calendar,
        Site       $site
    )
    {
        return $lib->not_spam.";".$lib->from_not_ad;
    }
}
